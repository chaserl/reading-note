# Summary

* [Introduction](README.md)
* [Effective Objective -C2.0](effective-objective-c20.md)
* [iOS 核心动画编程指南](ios-he-xin-dong-hua-bian-cheng-zhi-nan.md)
  * [CALayer Class](ios-he-xin-dong-hua-bian-cheng-zhi-nan/calayer-class.md)
  * [变换](ios-he-xin-dong-hua-bian-cheng-zhi-nan/bian-huan.md)
  * [特殊图层](ios-he-xin-dong-hua-bian-cheng-zhi-nan/te-shu-tu-ceng.md)
    * [CATiledLayer](ios-he-xin-dong-hua-bian-cheng-zhi-nan/catiledlayer.md)
    * [CAEAGLLayer](ios-he-xin-dong-hua-bian-cheng-zhi-nan/caeagllayer.md)
    * [CAShapeLayer](ios-he-xin-dong-hua-bian-cheng-zhi-nan/cashapelayer.md)
    * [CATextLayer](ios-he-xin-dong-hua-bian-cheng-zhi-nan/catextlayer.md)
    * [CAGradientLayer](ios-he-xin-dong-hua-bian-cheng-zhi-nan/cagradientlayer.md)
    * [CAReplicatorLayer](ios-he-xin-dong-hua-bian-cheng-zhi-nan/careplicatorlayer.md)
    * [CATransformLayer](ios-he-xin-dong-hua-bian-cheng-zhi-nan/catransformlayer.md)
    * [AVPlayerLayer](ios-he-xin-dong-hua-bian-cheng-zhi-nan/avplayerlayer.md)
    * CAEmitterLayer
  * 动画缓冲
  * [使用图层蒙版绘制有圆角有直角的 button](ios-he-xin-dong-hua-bian-cheng-zhi-nan/shi-yong-tu-ceng-meng-ban-hui-zhi-you-yuan-jiao-you-zhi-jiao-de-button.md)
* [AV Foundation开发秘籍——实践掌握iOS & OS X 应用的视听处理技术](av-foundationkai-fa-mi-ji-2014-2014-shi-jian-zhang-wo-ios-and-os-x-ying-yong-de-shi-ting-chu-li-ji-zhu.md)
* [Node.js 入门经典](nodejs-ru-men-jing-dian.md)
* [OpenGL ES应用开发实践指南iOS卷](opengl-esying-yong-kai-fa-shi-jian-zhi-nan-ios-juan.md)
  * [缓存](opengl-esying-yong-kai-fa-shi-jian-zhi-nan-ios-juan/huan-cun.md)
  * [GLKit](opengl-esying-yong-kai-fa-shi-jian-zhi-nan-ios-juan/glkit.md)

